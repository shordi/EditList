' Gambas class file

Export
''This control provides a mechanism for editing Arrays of type Date, Float, Integer, or String

Inherits UserControl

Public Const _Properties As String = "*,Value,List,ReadOnly,Type{Date;Float;Integer;String}=String,Border=True,Grid,Title,Unique=False,ShowControls=True"
Public Const _DefaultSize As String = "24,32"
Public Const _Similar As String = "ListBox"
Public Const _Group As String = "View"

Property Values As Variant[] ''Sets/returns the list of values to edit
Property Read Value As Variant ''Returns the Value of the selected item in the list
Property pnIcons As Object ''Returns the Icons panel to be able to change the image of the icons, add/hide buttons, etc.
Property Type As Integer ''Sets/returns the type of data to edit. Default is String
Property Border As Boolean ''Sets a flat border to the control
Property Title As String ''Sets a header for the list
Property ReadOnly As Boolean ''Override edit mode and only show the list
Property Unique As Boolean ''It does not allow to edit/add duplicate values to the list (But it does not check that the list itself already has them)
Property ShowControls As Boolean ''If false, buttons panel is not visible

Property Grid As Boolean ''Draw a line between each value
Property Index As Integer ''Returns the current number of the selected item (Note, this number varies as orders are made on the list)

'' This event is raised when an item is double-clicked.
Event Activate()
Event Changed(NewValue As Variant)

Private $hForm As FEditList

Public Sub _new()

  $hForm = New FEditList(Me)

End

Private Function Values_Read() As Variant[]

  Dim values As New Variant[]
  Dim n As Integer

  With $hForm.TblView
    For n = 0 To .Rows.Max
      values.Add(.[n, 0].tag) 'The tag is returned if the original list has been sorted/unsorted
    Next
  End With
  Return Values

End

Public Sub Changed(NewValue As Variant)

  Raise Changed(NewValue)

End

Private Sub Values_Write(Value As Variant[])

  $hForm.Values = Value

End

Private Function pnIcons_Read() As Object

  Return $hForm.pnIcons

End

Private Sub pnIcons_Write(Value As Object)

  $hForm.pnIcons = Value
  $hForm.EnableEditButtons()

End

Private Function ReadOnly_Read() As Boolean

  Return $hForm.ReadOnly

End

Private Sub ReadOnly_Write(Value As Boolean)

  $hForm.ReadOnly = Value

End

Private Function Type_Read() As Integer

  Return $hForm.Type

End

Private Sub Type_Write(Value As Integer)

  $hForm.Type = Value

End

Private Function Border_Read() As Boolean

  If $hForm.pnBorder.Border = Border.Plain Then
    Return True
  Else
    Return False
  Endif

End

Private Sub Border_Write(Value As Boolean)

  If Value Then
    $hForm.pnBorder.Border = Border.Plain
  Else
    $hForm.pnBorder.Border = Border.None
  Endif

End

Private Function Grid_Read() As Boolean

  Return $hForm.TblView.Grid

End

Private Sub Grid_Write(Value As Boolean)

  $hForm.TblView.Grid = Value

End

Private Function Index_Read() As Integer

  Return $hForm.TblView.Row

End

Private Sub Index_Write(Value As Integer)

  $hForm.TblView.Row = Value

End

Private Function Title_Read() As String

  Return $hForm.Header.Text

End

Private Sub Title_Write(Value As String)

  If Value Then
    $hForm.Header.parent.Visible = True
    $hForm.Header.Text = Value
  Else
    $hForm.Header.parent.Visible = False
  Endif

End

Private Function Value_Read() As Variant

  If $hForm.TblView.Row = -1 Then
    Return Null
  Else
    Return $hForm.TblView[$hForm.TblView.Row, 0].tag
  Endif

End

Private Function Unique_Read() As Boolean

  Return $hForm.Unique

End

Private Sub Unique_Write(Value As Boolean)

  $hForm.Unique = Value

End

Private Function ShowControls_Read() As Boolean

  Return $hForm.pnIcons.Visible

End

Private Sub ShowControls_Write(Value As Boolean)

  $hForm.pnIcons.Visible = Value

End
