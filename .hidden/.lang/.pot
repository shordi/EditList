#, fuzzy
msgid ""
msgstr ""
"#-#-#-#-#  #project.pot (EditList 3.18.2)  #-#-#-#-#\n"
"Project-Id-Version: EditList 3.18.2\n"
"POT-Creation-Date: 2023-04-24 08:49 UTC\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  EditList.pot ($(PACKAGE) $(VERSION))  #-#-#-#-#\n"
"Project-Id-Version: $(PACKAGE) $(VERSION)\n"
"POT-Creation-Date: 2023-04-24 08:44 UTC\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  FEditList.pot ($(PACKAGE) $(VERSION))  #-#-#-#-#\n"
"Project-Id-Version: $(PACKAGE) $(VERSION)\n"
"POT-Creation-Date: 2023-04-24 08:44 UTC\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  FMain.pot ($(PACKAGE) $(VERSION))  #-#-#-#-#\n"
"Project-Id-Version: $(PACKAGE) $(VERSION)\n"
"POT-Creation-Date: 2023-04-24 08:44 UTC\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "EditList"
msgstr ""

#: .project:2
msgid ""
"It is a control that allows you to edit arrays of different types: String, "
"Integer, Float or Date."
msgstr ""

#: FEditList.form:42
msgid "Ascending order"
msgstr ""

#: FEditList.form:49
msgid "Descending order"
msgstr ""

#: FEditList.form:56
msgid "Move item up"
msgstr ""

#: FEditList.form:63
msgid "Move item down"
msgstr ""

#: FEditList.form:74
msgid "Erase selected item"
msgstr ""

#: FEditList.form:81
msgid "Insert item"
msgstr ""

#: FEditList.form:88
msgid "Edit item"
msgstr ""

#: FEditList.form:95
msgid "Add new item"
msgstr ""

#: FMain.form:29
msgid "Get Value Selected"
msgstr ""

#: FMain.form:34
msgid "String"
msgstr ""

#: FMain.form:39
msgid "Float"
msgstr ""

#: FMain.form:44
msgid "Integer"
msgstr ""

#: FMain.form:49
msgid "Date"
msgstr ""

#: FMain.form:54
msgid "Change Icons"
msgstr ""

#: FMain.class:20
msgid "Selected "
msgstr ""

#: FMain.class:20
msgid " wich is of type "
msgstr ""

#: FMain.class:31
msgid "Item "
msgstr ""
